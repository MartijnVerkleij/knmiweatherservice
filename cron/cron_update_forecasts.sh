#!/bin/bash
echo "Pulling KNMI data"
# manage commands


export $(cat /gribviewer/.env | xargs)

echo "Pulling forecast data"
cd /gribviewer
/usr/bin/python3 manage.py pull_forecasts
#/usr/bin/python3 manage.py pull_observations
