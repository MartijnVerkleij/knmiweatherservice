#!/bin/bash
# manage commands


export $(cat /gribviewer/.env | xargs)

echo "Pulling observations data"
cd /gribviewer
#/usr/bin/python3 manage.py pull_forecasts
/usr/bin/python3 manage.py pull_observations
