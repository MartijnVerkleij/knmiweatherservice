from django.urls import path

from weather.views import LocationListView, LocationView, ForecastView, ForecastListView, IndexView

app_name = 'weather'

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('location/', LocationListView.as_view(), name='location_list'),
    path('location/<int:pk>', LocationView.as_view(), name='location'),
    path('forecast/', ForecastListView.as_view(), name='forecast_list'),
    path('forecast/<int:pk>', ForecastView.as_view(), name='forecast')
]