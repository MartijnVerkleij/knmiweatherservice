from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView, DetailView, ListView

from GribWeatherService.models import Location


class IndexView(TemplateView):
    template_name = 'weather/index.html'


class LocationListView(ListView):
    model = Location
    template_name = 'weather/location_list.html'


class LocationView(DetailView):
    template_name = 'weather/location.html'
    model = Location

    def get_object(self, queryset=None):
        if not queryset:
            queryset = Location.objects
        object = queryset.get(station_id=self.kwargs['pk'])
        # object.lat, object.lon = object.location.split(',')
        return object


class ForecastListView(ListView):
    model = Location
    template_name = 'weather/forecast_list.html'


class ForecastView(DetailView):

    template_name = 'weather/forecast.html'
    model = Location

    def get_object(self, queryset=None):
        if not queryset:
            queryset = Location.objects
        object = queryset.get(station_id=self.kwargs['pk'])
        # object.lat, object.lon = object.location.split(',')
        return object
