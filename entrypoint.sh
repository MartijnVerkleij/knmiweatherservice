#!/bin/sh

# Start CRON
service cron start

# Apply migrations to fresh database
/usr/bin/python3 manage.py migrate

# Create the cache table
/usr/bin/python3 manage.py createcachetable

# Run the webserver
/usr/bin/python3 manage.py runserver 0.0.0.0:8000