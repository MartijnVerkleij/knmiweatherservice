# Generated by Django 3.2 on 2021-04-21 20:00

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('location', models.CharField(max_length=200)),
                ('station_id', models.IntegerField(blank=True, null=True)),
                ('nearest_station_id', models.IntegerField(blank=True, null=True)),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(blank=True, max_length=10000, null=True)),
                ('active', models.BooleanField(default=True)),
            ],
        ),
    ]
