from django.apps import AppConfig


class GribweatherserviceConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'GribWeatherService'
