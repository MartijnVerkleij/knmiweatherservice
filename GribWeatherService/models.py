from django.contrib.gis.db.models import PointField
from django.db import models

# Create your models here.


class Location(models.Model):
    location = PointField()
    # location = models.CharField(max_length=200)  # lon/lat in string form
    station_id = models.IntegerField(blank=True, null=True)
    nearest_station_id = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=50)
    description = models.TextField(max_length=10000, blank=True, null=True)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'[{self.station_id}] {self.name}'
