import csv
import math
import os
from datetime import timedelta
from math import sqrt

from django.contrib.gis.geos import Point
from django.core.cache import cache, caches

from GribWeatherService.knmi_utils import fill_forecast_cache
from GribWeatherService.models import Location
from KNMIGribService.settings import BASE_DIR, HARMONIE_DATA_FOLDER, HARMONIE_FILENAME


def extract_bands_nearest_point(grib_file, lat, lon):
    """
    Gets the correspondent value to a latitude-longitude pair of coordinates in
    a grib file.

    :param grib_file:   path to the grib file in disk
    :param lat:         latitude
    :param lon:         longitude
    :param band_i:      band index
    :return:            scalar
    """
    observations_cache = caches['observations']

    ox, pw, xskew, oy, yskew, ph, time = cache.get('knmigrib', (None,) * 7)
    if not ox:
        fill_forecast_cache(grib_file)

        ox, pw, xskew, oy, yskew, ph, time = cache.get('knmigrib')

    # calculate the indices (row and column)
    i = -math.floor((oy - lat) / ph)
    j = math.floor((lon - ox) / pw)

    result = cache.get(f'{i}-{j}')

    if result is None:
        raise ValueError('Coordinates out of bounds')

    return result, time


def grib_data_nearest_point(lat, lon):
    grib_file = os.path.join(HARMONIE_DATA_FOLDER, HARMONIE_FILENAME)

    BANDS = 7

    data, time = extract_bands_nearest_point(grib_file, lat, lon)

    result = []
    for i in range(0, math.floor(len(data) / BANDS)):
        result.append({
            'dt': (time + timedelta(hours=i)).timestamp(),
            'time': time + timedelta(hours=i),
            'mslp': data[i * BANDS + 0],
            'rh': data[i * BANDS + 1],
            't': data[i * BANDS + 2],
            'u': data[i * BANDS + 3],
            'v': data[i * BANDS + 4],
            'ip': data[i * BANDS + 5],
            'ig': data[i * BANDS + 6],
            'ff': sqrt((data[i * BANDS + 3]**2) + (data[i * BANDS + 4]**2))
        })
    return result


def import_locations():
    with open(os.path.join(BASE_DIR, "stations.csv"), 'r') as file:
        csv_lines = list(csv.reader(file))
        for station_id, lat, lon, name in csv_lines:
            Location.objects.get_or_create(
                station_id=station_id,
                defaults={
                    'location': Point(float(lon), float(lat)),
                    'name': name,
                    'description': 'KNMI Weather Station\n\nImported automatically',
                }
            )