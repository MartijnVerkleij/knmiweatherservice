from django.contrib import admin

# Register your models here.
from leaflet.admin import LeafletGeoAdmin

from GribWeatherService.models import Location

admin.site.register(Location, LeafletGeoAdmin)