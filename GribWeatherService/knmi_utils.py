import logging
import math
import os
from datetime import datetime, timedelta
from pathlib import Path

import pytz
import requests
from cftime._cftime import num2date
from django.core.cache import cache, caches
from django.utils.timezone import make_aware, now
from netCDF4._netCDF4 import Dataset
from numpy import array
from osgeo import gdal

from GribWeatherService.models import Location

from KNMIGribService.settings import HARMONIE_DATA_FOLDER, KNMI_FORECASTS_CACHE_TIME, \
    NC_DATA_FOLDER, KNMI_NC_PREFIX, KNMI_OBSERVATIONS_CACHE_TIME, KNMI_10MIN_API_URL, \
    KNMI_10MIN_API_VERSION, KNMI_10MIN_DATASET_NAME, KNMI_10MIN_DATASET_VERSION, KNMI_API_KEY, \
    HARMONIE_FILENAME


def get_harmonie():
    print('Downloading GRIB file...')
    try:
        old_file_mtime = os.path.getmtime(os.path.join(HARMONIE_DATA_FOLDER, HARMONIE_FILENAME))
    except FileNotFoundError:
        old_file_mtime = 0
    os.system(f'cd {HARMONIE_DATA_FOLDER} && '
              'wget -N https://www.euroszeilen.utwente.nl/weer/grib/download/harmonie_xy.grb')

    new_file_mtime = os.path.getmtime(os.path.join(HARMONIE_DATA_FOLDER, HARMONIE_FILENAME))
    print("Dowload complete.")

    if old_file_mtime != new_file_mtime:
        fill_forecast_cache(grib_file=os.path.join(HARMONIE_DATA_FOLDER, HARMONIE_FILENAME))


def fill_forecast_cache(grib_file):

    # open the grib file, get the specified band and read it as an array
    ds = gdal.Open(grib_file, 0)

    bands = list(range(1, ds.RasterCount + 1))

    bands = [ds.GetRasterBand(band) for band in bands]

    arrs = array([band.ReadAsArray() for band in bands])
    reshaped_arr = arrs.transpose(1, 2, 0)
    time = datetime.fromtimestamp(int(bands[0].GetMetadata()['GRIB_VALID_TIME']
                                      .strip().split(' ')[0]))
    cache.set('knmigrib', ds.GetGeoTransform() + (time,), KNMI_FORECASTS_CACHE_TIME)

    for i in range(reshaped_arr.shape[0]):
        for j in range(reshaped_arr.shape[1]):
            # print(f'{i}-{j}')
            cache.set(f'{i}-{j}', reshaped_arr[i][j], KNMI_FORECASTS_CACHE_TIME)


def update_observations_cache(files=None):
    if files is None:
        soft_update = True
        print('Updating cache without argument')
        files = sorted(os.listdir(NC_DATA_FOLDER))[-24*6:]
    else:
        soft_update = False
        print('updating cache secifically for:')
        print(files)

    observations_cache = caches['observations']

    any_station = Location.objects.last()

    for file in files:
        if file.startswith(KNMI_NC_PREFIX):
            # print("Considering " + file)

            time = datetime.strptime(file[len(KNMI_NC_PREFIX):len(KNMI_NC_PREFIX) + 12], '%Y%m%d%H%M').timestamp()

            wn_values = observations_cache.get(f'{any_station.station_id}--{file}')

            if wn_values and soft_update:
                continue

            print("Opening " + file)
            nc_file = Dataset(os.path.join(NC_DATA_FOLDER, file), 'r')
            time_var = num2date(nc_file.variables['time'][:][0], units=nc_file.variables['time'].units)

            time_var = datetime.strptime(str(time_var), time_var.format).timestamp()

            print("Parsing " + file)

            description = {
                k: nc_file.variables[k].long_name for k in nc_file.variables if hasattr(nc_file.variables[k], 'long_name') and len(nc_file.variables[k].dimensions) == 2
            }
            observations_cache.set('description', description, KNMI_OBSERVATIONS_CACHE_TIME)
            units = {
                k: nc_file.variables[k].units for k in nc_file.variables if hasattr(nc_file.variables[k], 'units') and len(nc_file.variables[k].dimensions) == 2
            }
            observations_cache.set('units', units, KNMI_OBSERVATIONS_CACHE_TIME)

            for i in range(nc_file.dimensions['station'].size):
                location = Location.objects.filter(
                    station_id=nc_file.variables['station'][i]).first()

                if not location:
                    continue

                def subst(n):
                    return n if n != '--' else None

                wn_values = {'dt': time_var}

                wn_values.update({p: subst(nc_file.variables[p][i][0]) for p in
                             nc_file.variables if len(nc_file.variables[p].dimensions) == 2})

                u_wind = None
                v_wind = None
                if wn_values['ff'] and wn_values['dd']:
                    u_wind = wn_values['ff'] * math.cos(wn_values['dd'])
                    v_wind = wn_values['ff'] * math.sin(wn_values['dd'])

                wn_values.update({'u': u_wind, 'v': v_wind})

                observations_cache.set(f'{location.station_id}--{file}', wn_values, KNMI_OBSERVATIONS_CACHE_TIME)


def get_ncs():

    successfully_downloaded_files = []

    already_downloaded = []
    # already_downloaded = [f for f in sorted(os.listdir(NC_DATA_FOLDER)) if
    #                       f.startswith(NC_PREFIX)]
    # print("nc folder contents: " + str(sorted(os.listdir(NC_DATA_FOLDER))))

    timestamp = (datetime.utcnow() - timedelta(hours=24)).strftime('%Y%m%d%H%M')

    start_after_prefix = already_downloaded[-1] if already_downloaded else f'{KNMI_NC_PREFIX}{timestamp}'

    print('start_after_prefix: ' + start_after_prefix)
    if start_after_prefix and make_aware(datetime.strptime(
            start_after_prefix[len(KNMI_NC_PREFIX):len(KNMI_NC_PREFIX) + 12], '%Y%m%d%H%M'), pytz.UTC) + \
            timedelta(minutes=13) > now():
        print("Not updating")
        return  # Do nothing; data too new

    listing_response = requests.get(
        f"{KNMI_10MIN_API_URL}/{KNMI_10MIN_API_VERSION}/datasets/{KNMI_10MIN_DATASET_NAME}/versions/{KNMI_10MIN_DATASET_VERSION}/files",
        headers={"Authorization": KNMI_API_KEY},
        params={"maxKeys": 6*24, "startAfterFilename": start_after_prefix},
    ).json()

    files = listing_response.get('files')
    if not files:
        logging.error("Unable to fetch file list. API quota exceeded?")
        logging.error(listing_response)
        return
    for file_json in files:
        filename = file_json['filename']
        if filename in os.listdir(NC_DATA_FOLDER):
            if datetime.fromisoformat(file_json['lastModified']) <= make_aware(datetime.fromtimestamp(os.path.getmtime(os.path.join(NC_DATA_FOLDER, filename)))):
                # print(datetime.fromisoformat(file_json['lastModified']))
                # print(make_aware(datetime.fromtimestamp(
                #     os.path.getmtime(os.path.join(NC_DATA_FOLDER, filename)))))
                continue
        logging.info(f"Retrieve file with name: {filename}")
        endpoint = f"{KNMI_10MIN_API_URL}/{KNMI_10MIN_API_VERSION}/datasets/{KNMI_10MIN_DATASET_NAME}/versions/{KNMI_10MIN_DATASET_VERSION}/files/{filename}/url"
        get_file_response = requests.get(endpoint, headers={"Authorization": KNMI_API_KEY})
        if get_file_response.status_code != 200:
            logging.error("Unable to retrieve download url for file")
            logging.error(get_file_response.text)
            break

        download_url = get_file_response.json().get("temporaryDownloadUrl")
        dataset_file_response = requests.get(download_url)
        if dataset_file_response.status_code != 200:
            logging.error("Unable to download file using download URL")
            logging.error(dataset_file_response.text)
            break

        # Write dataset file to disk
        p = Path(os.path.join(NC_DATA_FOLDER, filename))
        p.write_bytes(dataset_file_response.content)
        logging.info(f"Successfully downloaded dataset file to {p}")
        successfully_downloaded_files.append(filename)

    return successfully_downloaded_files
