from django.urls import path

from GribWeatherService.views import get_forecast_by_lon_lat, get_observations_by_station, \
    get_stations, update_forecast_memcache

app_name = 'GribWeatherService'

urlpatterns = [
    path('forecast/<str:lon>/<str:lat>', get_forecast_by_lon_lat, name='get_forecast_by_lon_lat'),
    path('observations/<int:station_id>', get_observations_by_station, name='get_observations_by_station'),
    path('update_forecast', update_forecast_memcache, name='update_forecast_memcache'),
    path('stations', get_stations),
]