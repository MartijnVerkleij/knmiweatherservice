import os
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from django.core.cache import caches

from GribWeatherService.knmi_utils import update_observations_cache, fill_forecast_cache
from GribWeatherService.models import Location
from GribWeatherService.utils import grib_data_nearest_point
from KNMIGribService.settings import NC_DATA_FOLDER, KNMI_NC_PREFIX, HARMONIE_DATA_FOLDER, \
    HARMONIE_FILENAME

NETCDF_FILENAME = 'HA40_N55_202104130000_00000_GB.nc'

TOLERANCE = 0.2


def get_stations(request):
    return JsonResponse({l.station_id: {'name': l.name,
                                        'station_id': l.station_id,
                                        'description': l.description,
                                        'location': {'lon': l.location.x,
                                                     'lat': l.location.y}}
                         for l in Location.objects.all()})


def get_observations_by_station(request, station_id):


    observations_cache = caches['observations']

    # import_locations()

    station = get_object_or_404(Location, station_id=station_id)

    # new_ncs = get_ncs()
    # if new_ncs:
    #     update_cache(new_ncs)

    print(f'{station.station_id}')

    result = []

    should_update_cache = False

    for file in sorted(os.listdir(NC_DATA_FOLDER))[-24*6:]:
        if file.startswith(KNMI_NC_PREFIX):
            wn_values = observations_cache.get(f'{station.station_id}--{file}')
            if wn_values:
                result.append(wn_values)
            else:
                print('Missing data in cache for:' + file)
                should_update_cache = True

    if should_update_cache:
        update_observations_cache()

    if not result:
        return JsonResponse(data={'success': False, 'reason': 'There is no weather data available'
                                                              'at this time.'})

    description = observations_cache.get('description') or []
    units = observations_cache.get('units') or []

    return JsonResponse(data={'success': True,
                              'data': result,
                              'station': {'name': station.name,
                                          'station_id': station.station_id,
                                          'description': station.description,
                                          'location': {'lon': station.location.x,
                                                       'lat': station.location.y}
                                          },
                              'units': units,
                              'description': description

                              }

                        )


def get_forecast_by_lon_lat(request, lon, lat):
    try:
        float_lon = float(lon)
    except (ValueError, OverflowError) as e:
        return JsonResponse(data={'success': False, 'reason': 'There was a problem converting the '
                                                              '\'lon\' argument.'})
    try:
        float_lat = float(lat)
    except (ValueError, OverflowError) as e:
        return JsonResponse(data={'success': False, 'reason': 'There was a problem converting the '
                                                              '\'lat\' argument.'})

    try:
        data = grib_data_nearest_point(float_lat, float_lon)
    except ValueError as e:
        return JsonResponse(data={'success': False, 'reason': 'The requested coordinates were out '
                                                              'of bounds for the dataset.',
                                  'exception': str(e)})

    units = {
        'dt': "s",
        'mslp': "Pa",
        'rh': "%",
        't': "C",
        'u': "m/s",
        'v': "m/s",
        'ip': "kg/m²",
        'ig': "m/s",
        'ff': "m/s",
    }
    description = {
        'dt': "Time of prediction, in UTC, Unix timestamp",
        'time': "Time of prediction, in UTC",
        'mslp': "Pressure reduced to MSL",
        'rh': "Relative humidity",
        't': "Temperature",
        'u': "u-component of wind ",
        'v': "v-component of wind",
        'ip': "Total precipitation",
        'ig': "Wind Gusts",
        'ff': "Wind speed",

    }
    return JsonResponse(data={
        'success': True,
        'data': data,
        'units': units,
        'description': description
    })


def update_forecast_memcache(request):
    grib_file = os.path.join(HARMONIE_DATA_FOLDER, HARMONIE_FILENAME)
    fill_forecast_cache(grib_file)
    print("Updated forecast cache")
    return JsonResponse(data={
        'success': True,
    })
