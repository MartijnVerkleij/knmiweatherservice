from django.core.management import BaseCommand

from GribWeatherService.knmi_utils import get_ncs, update_observations_cache


class Command(BaseCommand):

    def handle(self, *args, **options):
        new_ncs = get_ncs()
        if new_ncs:
            update_observations_cache(new_ncs)
        else:
            update_observations_cache()

