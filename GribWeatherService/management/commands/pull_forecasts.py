import os

from django.core.management import BaseCommand

from GribWeatherService.knmi_utils import get_harmonie, fill_forecast_cache
from KNMIGribService.settings import HARMONIE_DATA_FOLDER, HARMONIE_FILENAME


class Command(BaseCommand):

    def handle(self, *args, **options):
        get_harmonie()
        grib_file = os.path.join(HARMONIE_DATA_FOLDER, HARMONIE_FILENAME)
        # fill_forecast_cache(grib_file)
