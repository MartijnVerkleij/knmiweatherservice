from django.core.management import BaseCommand

from GribWeatherService.utils import import_locations


class Command(BaseCommand):

    def handle(self, *args, **options):
        import_locations()
