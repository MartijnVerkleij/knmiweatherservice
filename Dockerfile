FROM registry.gitlab.com/dzeuros/grib-tools:latest

ENV PYTHONUNBUFFERED 1

RUN mkdir /gribviewer
WORKDIR /gribviewer
RUN apt-get update
RUN apt-get install python3-pip libgdal-dev libeccodes-tools cron -y
COPY requirements.txt /gribviewer/
RUN pip3 install -U pip
RUN pip3 install -r requirements.txt
COPY . /gribviewer/


# CRON
COPY cron/knmi_cron /etc/cron.d/knmi_cron
RUN chmod 0744 /etc/cron.d/knmi_cron
RUN crontab /etc/cron.d/knmi_cron


VOLUME /data/



EXPOSE 8000

ENTRYPOINT ["sh", "./entrypoint.sh"]
